/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.oxoop;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class OXOOP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char answer;
        Game game = new Game();
        game.showWelcome();
        game.newBorad();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showStat();
                System.out.println("Will you play again? ( yes(y) or no(n) ): ");
                answer = sc.next().charAt(0);
                if (answer == 'y') {
                    game.newBorad();
                } else {
                    System.out.println("Bye Bye");
                    break;
                }

            }
        }
    }

}
