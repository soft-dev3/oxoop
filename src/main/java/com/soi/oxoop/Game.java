/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soi.oxoop;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class Game {

    private Player o;
    private Player x;
    private Board board;
    private int row;
    private int col;
    Scanner sc = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');

    }

    public void newBorad() {
        this.board = new Board(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col:");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }

    public boolean isFinish() {
        if (board.isDraw() || board.isWin()) {
            return true;
        }
        return false;
    }
    public void showStat(){
        System.out.println(o);
        System.out.println(x);
    }

    public void showResult() {
        if (board.isDraw()) {
            System.out.println("Draw!!");
        } else {
            System.out.println(">>>" + board.getCurrentPlayer().getSymbol()+ " Win<<<");
        }
    }
}
